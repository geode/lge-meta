# Métadonnées de *La Grande Encyclopédie*

Ce dépôt regroupe des métadonnées sur *La Grande Encyclopédie, Inventaire
Raisonné des Sciences, des Lettres et des Arts*. Elles ont été extraites des
pages qui précèdent les articles dans le cadre du projet
[GEODE](https://geode-project.github.io/).

Le processus d'extraction a été semi-manuel: des
[données](https://nakala.fr/10.34847/nkl.74eb1xfd) issues du projet
[DiscoLGE](https://www.collexpersee.eu/projet/disco-lge/) pour une part et
d'autres publiées par
[Wikisource](https://fr.wikisource.org/wiki/La_Grande_Encyclop%C3%A9die,_inventaire_raisonn%C3%A9_des_sciences,_des_lettres,_et_des_arts#)
pour l'autre part, on été mises en forme manuellement.

## Contenu

Ces données consistent pour l'instant en:

- la liste des
  [domaines](https://gitlab.liris.cnrs.fr/geode/lge-meta/-/blob/main/domains.tsv?ref_type=heads)
  telle que décrite au tome premier par les auteurs, avec leur volumétrie
  prévisionnelle en nombre de lignes et de colonnes imprimées, partant sur une
  estimation totale de 25 volumes de 1 200 pages (il y en aura finalement 31)
- la liste des
  [abbréviations](https://gitlab.liris.cnrs.fr/geode/lge-meta/-/blob/main/abbréviations.tsv?ref_type=heads)
  utilisées, telle que présente dans la préface de l'œuvre disponible sur
  [Wikisource](https://fr.wikisource.org/wiki/La_Grande_Encyclop%C3%A9die,_inventaire_raisonn%C3%A9_des_sciences,_des_lettres,_et_des_arts/Pr%C3%A9face)
- la «Liste de MM. les collaborateurs de La Grande Encyclopédie», dans sa
  version «générale» publiée en fin d'ouvrage au tome 31 et répartie en 3
  fichiers pour suivre la présentation en trois groupes d'individus, qu'il est
  ensuite possible de combiner à loisir en fonction des besoins en fusionnant
  les fichiers utiles
    + le [Comité de
      direction](https://gitlab.liris.cnrs.fr/geode/lge-meta/-/blob/main/Liste%20de%20MM.%20les%20collaborateurs%20de%20La%20Grande%20Encyclop%C3%A9die/Comit%C3%A9%20de%20direction.tsv?ref_type=heads)
    + le [Secrétaire
      général](https://gitlab.liris.cnrs.fr/geode/lge-meta/-/blob/main/Liste%20de%20MM.%20les%20collaborateurs%20de%20La%20Grande%20Encyclop%C3%A9die/Secr%C3%A9taire%20g%C3%A9n%C3%A9ral.tsv?ref_type=heads)
      (une seule personne mais présentée à part et donc un fichier séparé dans
      ce système)
    + les
      [Contributeurs](https://gitlab.liris.cnrs.fr/geode/lge-meta/-/blob/main/Liste%20de%20MM.%20les%20collaborateurs%20de%20La%20Grande%20Encyclop%C3%A9die/Contributeurs.tsv?ref_type=heads)

## Réutilisation

Toutes les données diffusées dans ce dépôt le sont dans l'espoir qu'elles
pourront être utiles à de futures recherches au-delà du projet
[GEODE](https://geode-project.github.io/) lui-même et sont placées sous licence
Creative Commons
[Attribution-NonCommercial-ShareAlike](https://spdx.org/licenses/CC-BY-NC-SA-4.0.html#licenseText).
